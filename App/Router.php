<?php
namespace App;

use Closure;

/**
 * Class Router
 * @package App
 */
class Router {
    const DYNAMIC_ROUTE_PATTERN = "/\{(\w+)\}/";

    /**
     * @var string
     */
    protected $_url;

    /**
     * @var array
     */
    protected array $_routes = [];

    /**
     * Router constructor.
     */
    public function __construct() {
        $this->_url = strtolower($_GET['_url']);
    }

    /**
     * Add a new route to the application
     * @param $path
     * @param Closure $closure
     */
    public function add($path, Closure $closure) {
        $escapedPath = str_replace('/', '\/', $path);

        preg_match_all(self::DYNAMIC_ROUTE_PATTERN, $path, $matches);
        if(!empty($matches[1])){
            // @todo improve for data type matches (int, alfanumeric, etc.)
            $pattern = preg_replace(self::DYNAMIC_ROUTE_PATTERN, "(\w+)", $escapedPath);
        }

        $this->_routes[] = [
            'path' => strtolower($path),
            'pattern' => $pattern ?? $escapedPath,
            'params' => !empty($matches[1]) ? $matches[1] : null,
            'closure' => $closure
        ];

    }

    /**
     * Execute the closure on the route
     * @param $route
     * @param array $params
     */
    public function execute($route, array $params = []) {
        $route['closure'](...$params);
    }

    /**
     * Return Error 404 page if route match nothing
     */
    public function notFound() {
        header("HTTP/1.0 404 Not Found");
        View::render([
            'layout' => false,
            'page' => '404.php'
        ]);
        die();
    }

    /**
     * Dispatch requested route
     */
    public function dispatch() {
        $routeFound = false;
        foreach ($this->_routes as $key => $route) {
            preg_match('/^' . $route['pattern'] . '$/', $this->_url, $matches);

            if($matches) {
                $routeFound = true;
                array_shift($matches);
                $this->execute($route, $matches);
                break;
            }
        }

        if(!$routeFound) {
            $this->notFound();
        }
    }
}