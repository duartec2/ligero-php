<?php
namespace App;

use Exception;

/**
 * Class Loader
 * @package App
 */
class  Loader
{
    /**
     * @var array
     */
    protected static array $dirs = [];

    /**
     * @var int
     */
    protected static int $registered = 0;

    /**
     * Loader constructor.
     * @param array $dirs
     */
    public function __construct($dirs = []) {
        self::init($dirs);
    }

    /**
     * @param $file
     * @return bool
     */
    protected static function loadFile($file) : bool{
        if (file_exists($file)) {
            require_once $file;
            return true;
        }
        return false;
    }

    /**
     * @param $class
     * @return bool
     * @throws Exception
     */
    public static function autoload($class) {
        $success = false;
        $filename = strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php');

        foreach (self::$dirs as $start) {
            $file = $start . DIRECTORY_SEPARATOR . $filename;
            if (self::loadFile($file)) {
                $success = true;
                break;
            }
        }

        if (!$success) {
            if (!self::loadFile(__DIR__ . DIRECTORY_SEPARATOR . $filename)) {
                throw new Exception('No se puede cargar la clase:  ' . $class);
            }
        }
        return $success;
    }

    /**
     * @param mixed $dirs
     */
    public static function addDirs($dirs) {
        if (is_array($dirs)) {
            self::$dirs = array_merge(self::$dirs, $dirs);
        } else {
            self::$dirs[] = $dirs;
        }
    }

    /**
     * @param array $dirs
     */
    public static function init($dirs = []) {
        if ($dirs) {
            self::addDirs($dirs);
        }

        if (self::$registered == 0) {
            spl_autoload_register(__CLASS__ . '::autoload');
            self::$registered++;
        }
    }
}


