<?php
namespace App;

use Exception;

/**
 * Class View
 * @package App
 */
class View {
    const LAYOUT_PATH = ROOT_PATH . 'layout';
    const VIEWS_PATH = ROOT_PATH . 'views';
    const DEFAULT_LAYOUT = 'default.php';
    const DEFAULT_VIEW = 'home.php';

    /**
     * @var array
     */
    protected array $_vars;

    /**
     * @var string
     */
    protected string $_defaultView;

    /**
     * View constructor.
     *
     * Preset vars on configuration
     */
    public function __construct() {
        $this->setVars(CONFIG['view']['vars']);
        $this->_defaultView = self::DEFAULT_VIEW;
    }

    /**
     * Set view to render
     *
     * @param mixed $view
     * @param mixed $path
     * @throws Exception
     */
    public function setView($view = null, $path = null) {
        $view = $view ?? $this->_defaultView;

        $file = ($path ?? self::VIEWS_PATH) . '/'. $view;
        if (!file_exists($file)) {
            throw new Exception("No se encontró el archivo {$file} necesario para la interfaz de usuario.");
        }

        require $file;
    }

    /**
     * Get var in $vars
     * @param $var
     * @return mixed|null
     */
    public function __get($var) {
        return $this->_vars[$var] ?? null;
    }

    /**
     * Set var in $vars
     * @param $var
     * @param $val
     */
    public function __set($var, $val) {
        $this->_vars[$var] = $val;
    }

    /**
     * Check if var is set in vars
     * @param $var
     * @return bool
     */
    public function __isset($var) {
        return isset($this->_vars[$var]);
    }

    /**
     * Unset var in vars
     * @param $var
     */
    public function __unset($var) {
        unset($this->_vars[$var]);
    }

    /**
     * Set vars in view
     *
     * @param array $vars
     */
    public function setVars(array $vars) {
        foreach ($vars as $var => $value) {
            $this->{$var} = $value;
        }
    }

    /**
     * Estabish the default view
     * @param $view
     */
    public function setDefaultView($view) {
        $this->_defaultView = $view;
    }

    /**
     * Render Layout + Page
     *
     * @param mixed $settings
     * @param array $vars
     * @throws Exception
     */
    public static function render($settings = null, $vars = []) {
        switch($settings) {
            case is_array($settings):
                $layout = $settings['layout'] ?? self::DEFAULT_LAYOUT;
                $page = $settings['page'] ?? self::DEFAULT_VIEW;
                break;
            case is_string($settings):
                $layout = self::DEFAULT_LAYOUT;
                $page = $settings;
                break;
            case is_null($settings):
            default:
                $layout = self::DEFAULT_LAYOUT;
                $page = self::DEFAULT_VIEW;
                break;
        }

        $view = new self();
        $view->setVars($vars);

        //Set page
        $view->setDefaultView($page);

        //Set Layout
        $view->setView($layout, self::LAYOUT_PATH);
    }
}