<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo PUBLIC_PATH ?>/assets/css/bootstrap.min.css">

    <title><?php echo $this->page['title']?></title>
</head>
<body>
LAYOUT
<?php $this->setView(); ?>

<script src="<?php echo PUBLIC_PATH ?>/assets/js/jquery-3.5.1.min.js"></script>
<script src="<?php echo PUBLIC_PATH ?>/assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>