<?php
/**
 * @var \App\Router $app
 */

use App\View;
use Controller\User;

$app->add('/', function(){
   echo 'ROOT!!!';
});

$app->add('/book/{id}/do/{action}', function ($id, $action){
    //echo "La acción es {$action} al id {$id}";
    View::render('login.php', [
        'id' => $id
    ]);
});

$app->add('/user/login',function () {
    $user = new User();
    $user->login();
});

$app->add('/user/{id}', function ($id){

    View::render('home.php', [
        'id' => $id
    ]);
});