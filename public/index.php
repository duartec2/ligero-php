<?php
defined('ROOT_PATH') || define('ROOT_PATH', __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', dirname($_SERVER["PHP_SELF"]));
defined('CONFIG') || define('CONFIG', require_once ROOT_PATH . 'config/config.php');

use App\Loader;
use App\Router;

require ROOT_PATH . "App/Loader.php";
Loader::init(ROOT_PATH);

try {
    $app = new Router();
    include ROOT_PATH . "routes.php";
    $app->dispatch();
} catch (Exception $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}
